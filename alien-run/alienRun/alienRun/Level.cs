﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using System;

namespace alienRun
{
    class Level
     {
        // -------------------------------------
        // Data
        // -------------------------------------
        private Tile[,] tiles;

        private Texture2D boxTexture;
        private Texture2D bridgeTexture;
        private Texture2D spikeTexture;
        private Texture2D flagTexture;
        private Texture2D BronzeTexture;
        private Texture2D SilverTexture;
        private Texture2D GoldTexture;

        private bool completedLevel = false;

        private const int LEVEL_WIDTH = 100;
        private const int LEVEL_HEIGHT = 100;

        private const int TILE_WIDTH = 70;
        private const int TILE_HEIGHT = 70;

        // -------------------------------------
        // Behaviour
        // -------------------------------------
        public void LoadContent(ContentManager content)
        {
            // Creating a single copy of the tile texture that will be used by all tiles
            boxTexture = content.Load<Texture2D>("tiles/box");
            bridgeTexture = content.Load<Texture2D>("tiles/bridge");
            spikeTexture = content.Load<Texture2D>("tiles/spikes");
            flagTexture = content.Load<Texture2D>("tiles/goal");
            BronzeTexture = content.Load<Texture2D>("items/coinBronze");
            SilverTexture = content.Load<Texture2D>("items/coinSilver");
            GoldTexture = content.Load<Texture2D>("items/coinGold");

            SetupLevel();
           
        }
        // -------------------------------------
        public void SetupLevel()
        {
            completedLevel = false;

            tiles = new Tile[LEVEL_WIDTH, LEVEL_HEIGHT];

            //create solid box
            CreateBox(0, 8);
            CreateBox(1, 8);
            CreateBox(2, 8);
            CreateBox(3, 8);
            CreateBox(4, 8);
            CreateBox(5, 8);
            CreateBox(6, 8);
            CreateBox(7, 8);
            CreateBox(8, 8);

            // create platform
            CreateBridge(1, 5);
            CreateBridge(2, 5);
            CreateBridge(6, 5);
            CreateBridge(7, 5);
            CreateBridge(8, 5);
            CreateBridge(9, 5);

            // create platform
            CreateSpike(9, 10);
            CreateSpike(10, 10);
            CreateSpike(11, 10);
            CreateSpike(12, 10);
            CreateSpike(13, 10);

            CreateFlag(10,5);

            //create coins
            CreateBronzeCoin(5,7);

            //create coins
            CreateSilverCoin(6, 7);

            //create coins
            CreateGoldCoin(7, 7);




        }
        // -------------------------------------
        public void CreateBox(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(boxTexture, tilePosition, Tile.TileType.INPASSABLE);
            tiles[tileX, tileY] = newTile;
        }
        // -------------------------------------
        public void CreateBridge(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(bridgeTexture, tilePosition, Tile.TileType.PLATFORM);
            tiles[tileX, tileY] = newTile;
        }
        // -------------------------------------
        public void CreateSpike(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(spikeTexture, tilePosition, Tile.TileType.LETHAL);
            tiles[tileX, tileY] = newTile;
        }
        // -------------------------------------
        public void CreateFlag(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(flagTexture, tilePosition, Tile.TileType.GOAL);
            tiles[tileX, tileY] = newTile;
        }
        // -------------------------------------
        public void CreateBronzeCoin(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(BronzeTexture, tilePosition, Tile.TileType.POINTS,1);
            tiles[tileX, tileY] = newTile;
        }
        // -------------------------------------
        public void CreateSilverCoin(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(SilverTexture, tilePosition, Tile.TileType.POINTS,5);
            tiles[tileX, tileY] = newTile;
        }
        // -------------------------------------
        public void CreateGoldCoin(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(GoldTexture, tilePosition, Tile.TileType.POINTS,10);
            tiles[tileX, tileY] = newTile;
        }
        // -------------------------------------

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int x = 0; x < LEVEL_WIDTH; ++x)
            {
                for (int y = 0; y < LEVEL_HEIGHT; ++y)
                {
                    if (tiles[x, y] != null)
                        tiles[x, y].Draw(spriteBatch);
                }
            }
        }
        // -------------------------------------
        public List<Tile> GetTilesInBounds(Rectangle bounds)
        {
            // Create an empty list to fill with tiles
            List<Tile> tilesInBounds = new List<Tile>();

            // Determine the tile coordinate range for this rect
            int leftTile = (int)Math.Floor((float)bounds.Left / TILE_WIDTH);
            int rightTile = (int)Math.Ceiling((float)bounds.Right / TILE_WIDTH) - 1;
            int topTile = (int)Math.Floor((float)bounds.Top / TILE_HEIGHT);
            int bottomTile = (int)Math.Ceiling((float)bounds.Bottom / TILE_HEIGHT) - 1;

            // Loop through this range and add any tiles to the list
            for (int x = leftTile; x <= rightTile; ++x)
            {
                for (int y = topTile; y <= bottomTile; ++y)
                {
                    // only add tile if exists(not null) and visible
                    Tile thisTile = GetTile(x, y);

                    if (thisTile != null && thisTile.GetVisible()== true )
                        tilesInBounds.Add(thisTile);
                }
            }

            return tilesInBounds;
        }
        // -------------------------------------
        public Tile GetTile (int x, int y)
        {
            // check of we are out of bounds
            if (x < 0 || x >= LEVEL_WIDTH || y < 0 || y >= LEVEL_HEIGHT)
                return null;
            // otherwise we are within the bounds 
            return tiles[x, y];
        }
        // -------------------------------------
        public void CompleteLevel()
        {
            completedLevel = true;
        }
        // -------------------------------------
        public bool GetCompletedLevel()
        {
            return completedLevel;
        }
        // -------------------------------------
    }
}
