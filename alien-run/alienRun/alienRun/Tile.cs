﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace alienRun
{
    
    class Tile
    {
        // -------------------------------------
        // enums
        // -------------------------------------

            public enum TileType
        {
            INPASSABLE, // = 0 BLOCKS PLAYER MOVEMENT FROM ANY DIRECTION
            PLATFORM, // = 1 BLOCKS PLAYER MOVEMENT DOWNWARD ONLY
            LETHAL, // = 2 kills player on contact
            GOAL, // = 3 player wins level on contact
            POINTS, // = 4 player gets points on contact
        }


        // -------------------------------------
        // Data
        // -------------------------------------
        private Texture2D sprite;
        private Vector2 position;
        private TileType type;
        private int points = 0;
        private bool visible = true;


        // -------------------------------------
        // Behaviour
        // -------------------------------------
        public Tile(Texture2D newSprite, Vector2 newPosition, TileType newType, int newPoints = 0)
        {
            sprite = newSprite;
            position = newPosition;
            type = newType;
            points = newPoints;
        }
        // -------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            if(visible)
            spriteBatch.Draw(sprite, position, Color.White);
        }
        // -------------------------------------
        public Rectangle GetBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, sprite.Width, sprite.Height);
        }
        // -------------------------------------
        public TileType GetTileType()
        {
            return type;
        }
        // -------------------------------------
        public int GetPoints()
        {
            return points;
        }
        // -------------------------------------
        public void Hide()
        {
            visible = false;
        }
        // -------------------------------------
        public bool GetVisible()
        {
            return visible;
        }
        // -------------------------------------
    }

}
